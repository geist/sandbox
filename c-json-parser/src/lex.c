#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const unsigned FALSE_LEN = 5;
const unsigned TRUE_LEN = 4;
const unsigned NULL_LEN = 4;

typedef enum {
    SYMBOL, // For now, eventually we'll expand this to cover all the symbol types
    STRING,
    INT,
    FLOAT,
    BOOL,
    J_NULL,
} TokenType;

typedef struct {
    char* string;
    unsigned string_len;
    TokenType type;
} Token;

static void print_token(Token* token) {
    char* type_label;
    switch (token->type) {
        case SYMBOL:
            type_label = "Symbol";
            break;
        case STRING:
            type_label = "String";
            break;
        case INT:
            type_label = "Int";
            break;
        case FLOAT:
            type_label = "Float";
            break;
        case BOOL:
            type_label = "Bool";
            break;
        case J_NULL:
            type_label = "Null";
            break;
        default:
            type_label = "Unknown";
            break;
    }
    printf("'%s' (%s)\n", token->string, type_label);
}

static Token* new_token(char** source_string, unsigned string_len, TokenType type) {
    // TODO: Is there a cleaner way to do this? calloc might make more sense,
    // but we should research the perf implications in godbolt
    // TODO: If we're using the sentinel here, we don't need to store the length.
    char* new_string = malloc(sizeof(*new_string) * (string_len + 1));
    memcpy(new_string, *source_string, string_len);
    new_string[string_len] = '\0';

    // TODO: all mallocs are gonna need some error handling
    Token* token = malloc(sizeof(*token));
    token->string = new_string;
    token->string_len = string_len;
    token->type = type;

    return token;
}

static Token* lex_string(char** source_string) {
    // Skip opening quote
    *source_string += 1;

    unsigned string_length = 0;
    char* start = *source_string;

    // TODO: What to do about error in the case we dont find the closing quote
    // before we find end of source string?
    while (**source_string != '"' && **source_string != '\0') {
        string_length += 1;
        *source_string += 1;
    }

    // TODO: we're gonna need some error handling here
    if (string_length == 0) {
        return NULL;
    }

    // Skip closing quote
    *source_string += 1;

    Token* token = new_token(&start, string_length, STRING);
    return token;
}

static Token* lex_symbol(char** source_string) {
    Token* token = new_token(source_string, 1, SYMBOL);

    *source_string += 1;

    return token;
}

static Token* lex_number(char** source_string) {

    unsigned string_length = 0;
    char* start = *source_string;

    bool is_number = true;
    bool is_float = false;
    while (is_number) {
        if (**source_string == '.') {
            is_float = true;
        }

        switch (**source_string) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '-':
            case '.':
            case 'e': {
                string_length += 1;
                *source_string += 1;
                break;
            }
            default: {
                is_number = false;
                break;
            }
        }
    }

    // TODO: we're gonna need some error handling here
    if (string_length == 0) {
        return NULL;
    }

    TokenType type;
    if (is_float) {
        type = FLOAT;
    } else {
        type = INT;
    }

    Token* token = new_token(&start, string_length, type);
    return token;
}

static Token* lex_bool(char** source_string) {
    if (**source_string == 'f' && (strncmp(*source_string, "false", FALSE_LEN) == 0)) {
        Token* token = new_token(source_string, FALSE_LEN, BOOL);
        *source_string += FALSE_LEN;
        return token;
    } else if (**source_string == 't' && (strncmp(*source_string, "true", TRUE_LEN) == 0)) {
        Token* token = new_token(source_string, TRUE_LEN, BOOL);
        *source_string += TRUE_LEN;
        return token;
    }

    return NULL;
}

static Token* lex_null(char** source_string) {
    if (**source_string == 'n' && (strncmp(*source_string, "null", NULL_LEN) == 0)) {
        Token* token = new_token(source_string, NULL_LEN, J_NULL);
        *source_string += NULL_LEN;
        return token;
    }

    return NULL;
}


// TODO: Inline this in the lex functions? Or find some cleaner way to keep the memory tight
static void add_token(Token** tokens, unsigned* token_count, Token* token) {
    if (token == NULL) {
        return;
    }

    *token_count += 1;

    if (*tokens == NULL) {
        *tokens = token;
    } else {
        *tokens = realloc(*tokens, sizeof(Token) * *token_count);

        // (*tokens)[1] = *token;
        *((*tokens) + (*token_count - 1)) = *token;

        free(token);
    }

    return;
}

int lex(char* source_string) {
    unsigned tokens_count = 0;
    Token* tokens = NULL;

    while (*source_string != '\0') {
        switch (*source_string) {
            // Strings
            case '"': {
                Token* token = lex_string(&source_string);
                add_token(&tokens, &tokens_count, token);
                break;
            }
            // Numbers
            case '-':
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9': {
                Token* token = lex_number(&source_string);
                add_token(&tokens, &tokens_count, token);
            }
            // Syntax characters
            case ',':
            case ':':
            case '[':
            case ']':
            case '{':
            case '}': {
                Token* token = lex_symbol(&source_string);
                add_token(&tokens, &tokens_count, token);
                break;
            }
            // Bool
            case 't':
            case 'f': {
                Token* token = lex_bool(&source_string);
                add_token(&tokens, &tokens_count, token);
                break;
            }
            // Null
            case 'n': {
                Token* token = lex_null(&source_string);
                add_token(&tokens, &tokens_count, token);
                break;
            }
            // Whitespace
            case ' ':
            case '\t':
            case '\b':
            case '\n':
            case '\r': {
                source_string += 1;
                break;
            }
            default: {
                printf("ERROR, UNKNOWN CHARACTER: %c", *source_string);
                break;
            }
        }
    }

    // Print tokens and start freeing allocated memory
    for (int i = 0; i < tokens_count; i++) {
        Token* t = tokens + i;
        print_token(t);
        free(t->string);
    }

    free(tokens);

    return 0;
}
