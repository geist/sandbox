#include <stdio.h>

#include "lex.h"

int main(void) {
    lex("{\"foo\": 1, \"bar\": \"baz\", \"q\": null, \"a\": false, \"b\": true,\"aaa\":123.456,\"bbb\":-4e12}");

    /* char* output = lex("{\"foo\": 1}"); */
    /* puts(output); */

    return 0;
}
