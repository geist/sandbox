#include <stdio.h>
#include <errno.h>

#include "lex.h"

void log_fail(const char* func) {
    printf("%s failed!\n", func);
}

#define ASSERT(assertion) if (assertion != 0) { log_fail(__func__); return -1; }
#define TEST(test) if (test != 0) { errno = -1; }

int test_lex_simple(void) {
    ASSERT(lex("{\"foo\": 1}"));

    return 0;
}

int test_lex_all(void) {
    ASSERT(lex("{\"foo\": 1, \"bar\": \"baz\", \"q\": null, \"a\": false, \"b\": true,\"aaa\":123.456,\"bbb\":-4e12}"));

    return 0;
}

int main(void) {
    TEST(test_lex_simple());
    TEST(test_lex_all());

    return errno;
}
