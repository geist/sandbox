{ pkgs ? import <nixpkgs> {} }:
with pkgs; mkShell rec {
    nativeBuildInputs = with pkgs; [
      cmake
      gcc
      valgrind
    ];
}

