# Simple JSON Parser

https://notes.eatonphil.com/writing-a-simple-json-parser.html

## Status

Project skeleton in progress. Post skimmed. No code written yet.

## TODO

- [ ] Look into unit tests with CTest
https://stackoverflow.com/questions/50468620/what-does-enable-testing-do-in-cmake
https://cmake.org/cmake/help/latest/manual/ctest.1.html

## Usage

`cd ./build`
`make`
`./js` or `gdb ./js` etc.
