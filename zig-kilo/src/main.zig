const std = @import("std");
const ascii = std.ascii;
const fmt = std.fmt;
const fs = std.fs;
const io = std.io;
const mem = std.mem;
const os = std.os;

const StdOutBufWriter = io.BufferedWriter(4096, fs.File.Writer);

const Error = error{InvalidData};

const Vec2 = struct {
    x: usize,
    y: usize,
};

const EditorCommand = enum {
    cursorMoveLeft,
    cursorMoveDown,
    cursorMoveUp,
    cursorMoveRight,
    exit,
};

const Key = enum {
    h,
    j,
    k,
    l,
    ctrl_q,
    arrow_up,
    arrow_down,
    arrow_left,
    arrow_right,
};

const EditorConfig = struct {
    tty: fs.File,
    original_termios: os.termios,
    raw_termios: os.termios,
    screen_size: Vec2,
    stdin: fs.File.Reader,
    stdout: StdOutBufWriter,
    cursor_position: Vec2,

    const Self = @This();

    fn init() !Self {
        const tty: fs.File = try get_tty();
        const original_termios = try os.tcgetattr(tty.handle);
        errdefer os.tcsetattr(tty.handle, .FLUSH, original_termios) catch unreachable;

        const raw_termios = try enable_raw_mode(tty, original_termios);

        const stdin = io.getStdIn().reader();

        const stdout_file = io.getStdOut().writer();
        var stdout = io.bufferedWriter(stdout_file);

        const screen_size = try get_window_size(&tty, &stdout, &stdin);

        return Self{
            .original_termios = original_termios,
            .raw_termios = raw_termios,
            .tty = tty,
            .screen_size = screen_size,
            .stdin = stdin,
            .stdout = stdout,
            .cursor_position = Vec2{ .x = 0, .y = 0 },
        };
    }

    fn deinit(self: *Self) !void {
        try self.refresh_screen();
        try os.tcsetattr(self.tty.handle, .FLUSH, self.original_termios);
        self.tty.close();
    }

    fn draw_rows(self: *Self) !void {
        for (0..self.screen_size.y) |y| {
            _ = try self.stdout.write("~");

            _ = try self.stdout.write("\x1b[K"); // erase line starting at cursor
            if (y != self.screen_size.y - 1) {
                _ = try self.stdout.write("\r\n");
            }
        }
    }

    fn refresh_screen(self: *Self) !void {
        _ = try self.stdout.write("\x1b[?25l"); // hide cursor
        _ = try self.stdout.write("\x1b[H");

        try self.draw_rows();

        // draw cursor at last known position. y, x is the correct order here.
        try fmt.format(self.stdout.writer(), "\x1b[{d};{d}H", .{ self.cursor_position.y + 1, self.cursor_position.x + 1 });

        _ = try self.stdout.write("\x1b[?25h"); // show cursor

        try self.stdout.flush();

        return;
    }

    // TODO: I wonder how this would compare perf-wise to doing this without any casting
    fn move_cursor(self: *Self, x: isize, y: isize) void {
        const new_x = @max(0, @as(isize, @intCast(self.cursor_position.x)) + x);
        const new_y = @max(0, @as(isize, @intCast(self.cursor_position.y)) + y);

        self.cursor_position.x = @as(usize, new_x);
        self.cursor_position.y = @as(usize, new_y);
    }
};

fn get_tty() !fs.File {
    return fs.cwd().openFile("/dev/tty", .{ .mode = .read_write });
}

// TODO: Add comments to this function so we know what these flags all do
fn enable_raw_mode(tty: fs.File, termios: os.termios) !os.termios {
    var raw = termios;

    raw.iflag &= ~@as(os.system.tcflag_t, os.system.BRKINT | os.system.ICRNL | os.system.INPCK | os.system.ISTRIP | os.system.IXON);

    raw.oflag &= ~@as(os.system.tcflag_t, os.system.OPOST);

    raw.cflag |= os.system.CS8;

    raw.lflag &= ~@as(os.system.tcflag_t, os.system.ECHO | os.system.ICANON | os.system.IEXTEN | os.system.ISIG);

    raw.cc[os.system.V.MIN] = 0;
    raw.cc[os.system.V.TIME] = 1;

    try os.tcsetattr(tty.handle, .FLUSH, raw);

    return raw;
}

fn ctrl_key(comptime char: u8) u8 {
    return char & 0b00011111;
}

fn editor_parse_byte(stdin: *const fs.File.Reader) !?u8 {
    return stdin.readByte() catch |err| {
        switch (err) {
            error.EndOfStream => return null,
            else => |e| return e,
        }

        return null;
    };
}

fn editor_read_key(stdin: *const fs.File.Reader) !?Key {
    var char = try editor_parse_byte(stdin) orelse return null;

    return switch (char) {
        'h' => .h,
        'j' => .j,
        'k' => .k,
        'l' => .l,
        ctrl_key('q') => .ctrl_q,
        else => null,
    };
}

fn get_window_size(tty: *const fs.File, stdout: *StdOutBufWriter, stdin: *const fs.File.Reader) !Vec2 {
    var size = mem.zeroes(os.system.winsize);

    const err = os.system.ioctl(tty.handle, os.system.T.IOCGWINSZ, &size);
    if (os.errno(err) != .SUCCESS or size.ws_col == 0 or size.ws_row == 0) {
        // TODO: return the cursor to its original position
        _ = try stdout.write("\x1b[999C\x1b[999B");
        try stdout.flush();
        return get_cursor_position(stdin, stdout);
    }

    return Vec2{
        .x = size.ws_col,
        .y = size.ws_row,
    };
}

fn get_cursor_position(stdin: *const fs.File.Reader, stdout: *StdOutBufWriter) !Vec2 {
    var buf: [32]u8 = mem.zeroes([32]u8);

    _ = try stdout.write("\x1b[6n");
    try stdout.flush();

    var buf_len = try stdin.readAll(&buf);

    if (buf[0] != '\x1b' or buf[1] != '[' or buf[buf_len - 1] != 'R') {
        return Error.InvalidData;
    }

    var split = mem.split(u8, buf[2 .. buf_len - 1], ";");
    var x_bytes = split.next().?;
    var y_bytes = split.next().?;

    return Vec2{
        .x = try fmt.parseUnsigned(usize, x_bytes, 10),
        .y = try fmt.parseUnsigned(usize, y_bytes, 10),
    };
}

fn keypress_to_command(key: Key) ?EditorCommand {
    return switch (key) {
        .h => .cursorMoveLeft,
        .j => .cursorMoveDown,
        .k => .cursorMoveUp,
        .l => .cursorMoveRight,
        .ctrl_q => .exit,
        .arrow_up => .cursorMoveUp,
        .arrow_down => .cursorMoveDown,
        .arrow_left => .cursorMoveLeft,
        .arrow_right => .cursorMoveRight,
        // else => null,
    };
}

pub fn main() !void {
    var editor_config = try EditorConfig.init();
    defer editor_config.deinit() catch unreachable;

    while (true) {
        try editor_config.refresh_screen();

        var char = try editor_read_key(&editor_config.stdin);
        if (char) |c| {
            if (keypress_to_command(c)) |command| {
                switch (command) {
                    .cursorMoveLeft => editor_config.move_cursor(-1, 0),
                    .cursorMoveUp => editor_config.move_cursor(0, -1),
                    .cursorMoveDown => editor_config.move_cursor(0, 1),
                    .cursorMoveRight => editor_config.move_cursor(1, 0),
                    .exit => break,
                }
            }
        }
    }
}
