# Write a Hash Table

https://github.com/jamesroutley/write-a-hash-table

Author's final code: https://github.com/jamesroutley/algorithms-and-data-structures/tree/master/hash-table/src

(it has bugs)

## Status

Functionally complete, added basic acceptance criteria in `main` in lieu of a full unit test setup. Good first intro to C, although the tutorial was buggy and missed defining quite a lot.

## Usage

`cd ./build/{debug|release}`
`make`
`./ht` or `gdb ./ht` etc.
