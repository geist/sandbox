#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "hash_table.h"
#include "prime.h"

// we can't just remove an item from the table if we delete it because that
// would break the collision chain. therefore, we'll simply set it to deleted
static ht_item HT_DELETED_ITEM = {NULL, NULL};

static int HT_INITIAL_BASE_SIZE = 53;

static const int HT_PRIME_1 = 151;
static const int HT_PRIME_2 = 163;

// initialize a new item
static ht_item* ht_new_item(const char* k, const char* v) {
    // allocate the memory for the item
    ht_item* i = malloc(sizeof(ht_item));
    // duplicate k into key
    i->key = strdup(k);
    // duplicate v into value
    i->value = strdup(v);
    return i;
}

static ht_hash_table* ht_new_sized(const int base_size) {
    // allocate the memory for the table
    ht_hash_table* ht = malloc(sizeof(ht_hash_table));
    // set the base size to the given base size
    ht->base_size = base_size;

    // find the next prime nearest the base size as the size
    ht->size = next_prime(ht->base_size);

    ht->count = 0;
    ht->items = calloc((size_t)ht->size, sizeof(ht_item*));
    return ht;
}

// initialize a new hash table
ht_hash_table* ht_new() {
    return ht_new_sized(HT_INITIAL_BASE_SIZE);
}

// frees the key, the value and then the item itself
static void ht_del_item(ht_item* i) {
    free(i->key);
    free(i->value);
    free(i);
}

// frees a hash table
void ht_del_hash_table(ht_hash_table* ht) {
    // delete each item in the table
    for (int i = 0; i < ht->size; i++) {
        ht_item* item = ht->items[i];
        if (item != NULL && item != &HT_DELETED_ITEM) {
            ht_del_item(item);
        }
    }
    // free the items memory
    free(ht->items);
    // free the table itself
    free(ht);
}

// hash function, take a string, a which should be a prime larger than the size of the alphabet
// we are using ascii which gives us an alphabet size of 128. m is number of buckets
static int ht_hash(const char* s, const int a, const int m) {
    long hash = 0;
    // length of the string
    const int len_s = strlen(s);
    // iterate over the range of the string
    for (int i = 0; i < len_s; i++) {
        // add a**(len_s - (i+1)) * the character at this index in the string
        hash += (long)pow(a, len_s - (i+1)) * s[i];
        // mod m
        hash = hash % m;
    }
    return (int)hash;
}

// use two hash functions for collisions
static int ht_get_hash(
    const char* s, const int num_buckets, const int attempt
) {
    const int hash_a = ht_hash(s, HT_PRIME_1, num_buckets);
    const int hash_b = ht_hash(s, HT_PRIME_2, num_buckets);
    return (hash_a + (attempt * (hash_b + 1))) % num_buckets;
}


// resize a hash table
// or more accurately, create a new one, copy over the contents, then swap the
// attributes and delete the "new" (actually the old one) one
static void ht_resize(ht_hash_table* ht, const int base_size) {
    // if its smaller than the initial base size, we dont resize it any further
    if (base_size < HT_INITIAL_BASE_SIZE) {
        return;
    }

    // create the new table
    ht_hash_table* new_ht = ht_new_sized(base_size);
    // iterate the items
    for (int i = 0; i < ht->size; i++) {
        ht_item* item = ht->items[i];
        // if the item isnt null or deleted
        if (item != NULL && item != &HT_DELETED_ITEM) {
            // insert it into the new table
            ht_insert(new_ht, item->key, item->value);
        }
    }

    // set the old base_size to the new base size
    ht->base_size = new_ht->base_size;
    // set the old count the new count
    ht->count = new_ht->count;

    // to delete new_ht, we give it ht's size and items
    const int tmp_size = ht->size;
    ht->size = new_ht->size;
    new_ht->size = tmp_size;

    ht_item** tmp_items = ht->items;
    ht->items = new_ht->items;
    new_ht->items = tmp_items;

    ht_del_hash_table(new_ht);
}

static void ht_resize_up(ht_hash_table* ht) {
    const int new_size = ht->base_size * 2;
    ht_resize(ht, new_size);
}

static void ht_resize_down(ht_hash_table* ht) {
    const int new_size = ht->base_size / 2;
    ht_resize(ht, new_size);
}

// insert an item into the table
void ht_insert(ht_hash_table* ht, const char* key, const char* value) {
    // if the load is greater than 70%, resize up
    const int load = ht->count * 100 / ht->size;
    if (load > 70) {
        ht_resize_up(ht);
    }

    // create a new item with the given key, value
    ht_item* item = ht_new_item(key, value);
    // get the table index of the new item based on the item's key
    int index = ht_get_hash(item->key, ht->size, 0);
    // check if there is an existing item at this index
    ht_item* cur_item = ht->items[index];
    // if the current index is not null
    int i = 1;
    while (cur_item != NULL && cur_item != &HT_DELETED_ITEM) {
        // if this key already exists in this table
        if (strcmp(cur_item->key, key) == 0) {
            // delete the existing item
            ht_del_item(cur_item);
            // and replace it with the new one
            ht->items[index] = item;
            return;
        }
        // get a hash with i / attempt # as a nonce as a new index
        index = ht_get_hash(item->key, ht->size, i);
        // set this new index as the current item
        cur_item = ht->items[index];
        // increment and check again
        i++;
    }
    // if we've found an empty index, set that index to this item
    ht->items[index] = item;
    // increment the count of items in the table
    ht->count++;
}

char* ht_search(ht_hash_table* ht, const char* key) {
    // get the index as the hash of the given key
    int index = ht_get_hash(key, ht->size, 0);
    // get the item at that index
    ht_item* item = ht->items[index];
    // check if that item exists
    int i = 1;
    while (item != NULL) {
        // if the key at the current index matches the one we've found
        if (item != &HT_DELETED_ITEM && strcmp(item->key, key) == 0) {
            // return the value, since we've found the one we're looking for
            return item->value;
        }
        // check the next nonce to see if it's there
        index = ht_get_hash(key, ht->size, i);
        item = ht->items[index];
        i++;
    }
    // we did not find the item
    return NULL;
}

void ht_delete(ht_hash_table* ht, const char* key) {
    // if the load is less than 10%, resize down
    const int load = ht->count * 100 / ht->size;
    if (load < 10) {
        ht_resize_down(ht);
    }

    // check the index for the given key
    int index = ht_get_hash(key, ht->size, 0);
    ht_item* item = ht->items[index];
    // if it does exist, we want to delete it
    int i = 1;
    while (item != NULL) {
        // if the found item is NOT deleted yet..
        if (item != &HT_DELETED_ITEM) {
            // ..and the key matches the one we're looking for
            if (strcmp(item->key, key) == 0) {
                // delete the item
                ht_del_item(item);
                // and replace it with the deleted placeholder
                ht->items[index] = &HT_DELETED_ITEM;
                // decrement the total count
                ht->count--;
                return;
            }
        }
        // check the next nonce
        index = ht_get_hash(key, ht->size, i);
        item = ht->items[index];
        i++;
    }
}

