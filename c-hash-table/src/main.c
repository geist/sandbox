#include <assert.h>
#include <string.h>

#include "hash_table.h"

int main() {
    // create new hash table
    ht_hash_table* ht = ht_new();

    assert(ht->count == 0);

    // insert an item
    ht_insert(ht, "1", "one");
    assert(ht->count == 1);

    // insert another
    ht_insert(ht, "2", "two");
    assert(ht->count == 2);

    // search the table
    char* value1 = ht_search(ht, "1");
    assert(strcmp(value1, "one") == 0);

    char* value2 = ht_search(ht, "2");
    assert(strcmp(value2, "two") == 0);

    char* value3 = ht_search(ht, "nonexistent");
    assert(value3 == NULL);

    // update an item
    ht_insert(ht, "1", "neo");
    assert(ht->count == 2);

    char* value4 = ht_search(ht, "1");
    assert(strcmp(value4, "neo") == 0);

    // remove an item
    ht_delete(ht, "2");
    assert(ht->count == 1);

    char* value5 = ht_search(ht, "2");
    assert(value5 == NULL);

    // try removing a non-existent item
    ht_delete(ht, "nonexistent");
    assert(ht->count == 1);

    ht_del_hash_table(ht);
}
