# Simple Node-API Example

`./build.sh`

`npm test` (this also runs build.sh)

### Goal

Write C code that can be used in Javascript in the most minimal fashion. 0 dependencies besides C stdlib and NodeJS itself. The example code is a basic segfault catcher since, by default, Node-API crashes are not picked up by NodeJS. This can be shipped as a pre-compiled binary, meaning no external dependencies when `npm install` is executed.

### Status

Functionally complete. To improve this further would be to polish up the backtrace printing, and setting up cross-compilation, so the binaries could be shipped as npm dependencies with 0 install dependencies. No python, no build systems, just a simple binary.
