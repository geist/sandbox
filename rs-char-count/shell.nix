{ pkgs ? import <nixpkgs> {} }:
with pkgs; mkShell rec {
    buildInputs = with pkgs; [
        cargo
        clippy
        rust-analyzer
        rustc
        rustfmt
    ];
}

