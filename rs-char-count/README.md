# Char-Count

A quick little tool to count all the characters in files with the matching filetypes. Used to create a ranked list of which symbols are most typed in a codebase.

## Status

Functionally complete, got what I wanted

## Usage

`cargo run ./my-dir`
