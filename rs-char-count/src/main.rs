use std::{
    collections::HashMap,
    env,
    fs::{read_dir, File},
    io::{BufRead, BufReader},
    path::PathBuf,
};

const IGNORED_DIRS: [&str; 4] = [".git", "node_modules", "target", "build"];

const FILETYPES: [&str; 2] = ["rs", "ts"];
// const FILETYPES: [&str; 1] = ["rs"];

/// Recursively traverses a directory and counts all characters contained in any files ending in
/// the given filetypes.
fn main() {
    let args: Vec<String> = env::args().collect();
    println!("args {:?}", args);

    if args.len() != 2 {
        println!("Need a path to a folder as an argument");
    }

    let root_dir = &args[1];

    let mut paths: Vec<PathBuf> = vec![PathBuf::from(root_dir)];

    let mut chars: HashMap<char, u32> = HashMap::new();

    while !paths.is_empty() {
        let path = paths.pop().unwrap();
        let dir_contents = read_dir(path).unwrap();

        for entry in dir_contents {
            let entry = entry.unwrap();

            let file_type = entry.file_type().unwrap();
            if file_type.is_dir() {
                let name = entry.file_name().into_string().unwrap();
                if !IGNORED_DIRS.contains(&name.as_str()) {
                    paths.push(entry.path());
                }

                // An entry cannot be both a dir and a file so lets just save ourselves some work
                continue;
            }

            if file_type.is_file() {
                let name = entry.file_name().into_string().unwrap();
                let mut split_name = name.split('.');
                let extension = split_name.next_back().unwrap();

                if FILETYPES.contains(&extension) {
                    let file = File::open(entry.path()).unwrap();
                    let reader = BufReader::new(file);

                    for line in reader.lines() {
                        for ch in line.unwrap().chars() {
                            let count = chars.entry(ch).or_insert(0);
                            *count += 1;
                        }
                    }
                }
            }
        }
    }

    let mut hash_vec: Vec<(&char, &u32)> = chars.iter().collect();
    hash_vec.sort_by(|a, b| b.1.cmp(a.1));
    for (ch, count) in hash_vec {
        println!("{} occurred {} times", ch, count);
    }
}
