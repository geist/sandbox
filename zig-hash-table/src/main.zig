const std = @import("std");
const mem = std.mem;
const Allocator = mem.Allocator;
const testing = std.testing;
const expect = testing.expect;
const expectEqual = testing.expectEqual;

const prime = @import("prime.zig");

const HT_PRIME_1: u32 = 151;
const HT_PRIME_2: u32 = 163;

const INITIAL_BASE_SIZE: u32 = 50;

const DELETED_ITEM = Item.new("", "");

const Item = struct {
    key: []const u8,
    value: []const u8,

    fn new(key: []const u8, value: []const u8) Item {
        return Item{ .key = key, .value = value };
    }
};

const HashTable = struct {
    base_size: u32,
    size: u32,
    count: u32,
    items: []?Item,
    allocator: Allocator,

    pub fn new(allocator: Allocator) !*HashTable {
        return HashTable.new_sized(allocator, INITIAL_BASE_SIZE);
    }

    pub fn new_sized(allocator: Allocator, base_size: u32) !*HashTable {
        const table = try allocator.create(HashTable);

        table.allocator = allocator;
        table.base_size = base_size;
        table.size = prime.next_prime(base_size);
        table.count = 0;

        table.items = try allocator.alloc(?Item, table.size);
        for (table.items) |*t| {
            t.* = null;
        }

        return table;
    }

    pub fn deinit(self: *HashTable) void {
        self.allocator.free(self.items);
        self.allocator.destroy(self);
    }

    pub fn insert(self: *HashTable, key: []const u8, value: []const u8) void {
        // TODO: I dont love the idea of automatic resizing here.  Make this
        // manual? If we make it manual, we dont need to store the allocator on
        // the HashTable. Maybe we shouldnt anyway, what is the idiomatic thing
        // to do here?
        // TODO: Test this
        // if (self.count / self.size > 70) {
        //     try self.resize_up();
        // }

        const item = Item.new(key, value);

        var index = self.get_hash(item.key, 0);
        var current_item = self.items[index];

        var i: u8 = 1;
        while (current_item != null and !std.meta.eql(current_item.?, DELETED_ITEM)) : (i += 1) {
            if (mem.eql(u8, current_item.?.key, key)) {
                self.items[index] = item;
                return;
            }
            index = self.get_hash(item.key, i);
            current_item = self.items[index];
        }

        self.items[index] = item;
        self.count += 1;
    }

    pub fn search(self: *HashTable, key: []const u8) ?[]const u8 {
        var index = self.get_hash(key, 0);
        var item = self.items[index];

        var i: u8 = 1;
        while (item != null) : (i += 1) {
            if (!std.meta.eql(item.?, DELETED_ITEM) and mem.eql(u8, item.?.key, key)) {
                return item.?.value;
            }

            index = self.get_hash(key, i);
            item = self.items[index];
        }

        return null;
    }

    pub fn delete(self: *HashTable, key: []const u8) void {
        // TODO: Test this
        // if (self.count / self.size < 10) {
        //     try self.resize_down();
        // }

        var index = self.get_hash(key, 0);
        var item = self.items[index];

        var i: u8 = 1;
        while (item != null) : (i += 1) {
            if (!std.meta.eql(item.?, DELETED_ITEM) and mem.eql(u8, item.?.key, key)) {
                self.items[index] = DELETED_ITEM;
                self.count -= 1;
                break; // TODO: We dont want this if we allow multiple inserts
            }

            index = self.get_hash(key, i);
            item = self.items[index];
        }
    }

    fn hash(self: *HashTable, string: []const u8, a: u32) u32 {
        var hashed: u64 = 0;
        var len = string.len;
        var i: u64 = 0;
        while (i < len) : (i += 1) {
            // TODO:
            // for (0..len) |i| {
            hashed += std.math.pow(u64, a, len - (i + 1)) * string[i];
            hashed %= self.size;
        }

        return @intCast(u32, hashed);
    }

    // TODO: Only hash b if needed?
    fn get_hash(self: *HashTable, string: []const u8, attempt: u32) u32 {
        const hash_a = self.hash(string, HT_PRIME_1);
        const hash_b = self.hash(string, HT_PRIME_2);

        return (hash_a + (attempt * (hash_b + 1))) % self.size;
    }

    fn resize_up(self: *HashTable) !void {
        try self.resize(self.base_size * 2);
    }

    fn resize_down(self: *HashTable) !void {
        try self.resize(self.base_size / 2);
    }

    fn resize(self: *HashTable, new_base_size: u32) !void {
        if (new_base_size < INITIAL_BASE_SIZE) {
            return;
        }

        var new_table = try HashTable.new_sized(self.allocator, new_base_size);

        for (self.items) |item| {
            // TODO: is std.meta.eql the most performant thing? is a
            // Item.deleted bool better?
            if (item != null and !std.meta.eql(item.?, DELETED_ITEM)) {
                new_table.insert(item.?.key, item.?.value);
            }
        }

        self.base_size = new_table.base_size;
        self.count = new_table.count;

        // TODO: do we really need to swap the sizes? dont think so
        var tmp_size = self.size;
        self.size = new_table.size;
        new_table.size = tmp_size;

        var tmp_items = self.items;
        self.items = new_table.items;
        new_table.items = tmp_items;

        new_table.deinit();
    }
};

export fn add(a: i32, b: i32) i32 {
    return a + b;
}

test "Item new" {
    var i: u8 = 0;
    while (i < 5) : (i += 1) {
        // TODO:
        // for (0..5) |v| {
        var key = [_]u8{ i, 'e', 'l', 'l', 'o' };
        var value = [_]u8{ i, 'o', 'r', 'l', 'd' };

        const item = Item.new(&key, &value);

        try expect(mem.eql(u8, item.key, &[_]u8{ i, 'e', 'l', 'l', 'o' }));
        try expect(mem.eql(u8, item.value, &[_]u8{ i, 'o', 'r', 'l', 'd' }));
    }
}

// TODO: Hashtable tests
// double delete
// double insert (might already have)
// insert delete re-insert
// operations on empty tables
// any other potentially faulty permutations
// re-write some of these to use the proper methods instead of hacks
test "HashTable new" {
    const allocator = testing.allocator;

    const table = try HashTable.new(allocator);
    defer table.deinit();

    try expect(table.base_size == 50);
    try expect(table.size == 53);
}

test "HashTablew new_sized" {
    const allocator = testing.allocator;

    const table = try HashTable.new_sized(allocator, 100);
    defer table.deinit();

    try expect(table.base_size == 100);
    try expect(table.size == 101);

    for (table.items) |t| {
        try expect(t == null);
    }
}

test "HashTable insert" {
    const allocator = testing.allocator;

    const table = try HashTable.new(allocator);
    defer table.deinit();

    table.insert("hello", "world1");
    try expect(table.count == 1);

    // Test updating an existing value
    table.insert("hello", "world2");
    try expect(table.count == 1);

    table.insert("foo", "bar");
    try expect(table.count == 2);

    var index1 = table.get_hash("hello", 0);
    var index2 = table.get_hash("foo", 0);

    try expect(index1 != index2);
    try expect(mem.eql(u8, table.items[index1].?.value, "world2"));
    try expect(mem.eql(u8, table.items[index2].?.value, "bar"));
}

test "HashTable search" {
    const allocator = testing.allocator;

    const table = try HashTable.new(allocator);
    defer table.deinit();

    table.insert("hello", "world1");
    table.insert("foo", "bar");

    var value1 = table.search("hello");
    var value2 = table.search("foo");

    try expect(mem.eql(u8, value1.?, "world1"));
    try expect(mem.eql(u8, value2.?, "bar"));
}

test "HashTable delete" {
    const allocator = testing.allocator;

    const table = try HashTable.new(allocator);
    defer table.deinit();

    // Intentional collision to test that we don't break the collision chain on
    // delete
    var collision: []const u8 = &[_]u8{50};

    table.insert("hello", "world1");
    table.insert("foo", "bar");
    table.insert(collision, "lol");

    try expect(table.count == 3);
    try expect(table.search("hello") != null);
    try expect(table.search("foo") != null);
    try expect(mem.eql(u8, table.search(collision).?, "lol"));

    table.delete("hello");

    try expect(table.count == 2);
    try expect(table.search("hello") == null);
    try expect(table.search("foo") != null);
    try expect(mem.eql(u8, table.search(collision).?, "lol"));
    // TODO: Delete then re-insert
}

test "HashTable hash" {
    const allocator = testing.allocator;

    const table = try HashTable.new(allocator);
    defer table.deinit();

    try expect(table.hash("cat", 151) == 5);
    try expect(table.hash("cat", 163) == 21);
}

test "HashTable get_hash" {
    const allocator = testing.allocator;

    const table = try HashTable.new(allocator);
    defer table.deinit();

    try expect(table.get_hash("cat", 0) == 5);
    try expect(table.get_hash("cat", 1) == 27);
}

test "HashTable resize_up" {
    const allocator = testing.allocator;

    const table = try HashTable.new(allocator);
    defer table.deinit();

    table.insert("hello", "world");
    table.insert("foo", "bar");
    // TODO: Collisions can be computed by using get_hash instead. This will
    // make changing the hash algo less painful
    table.insert(&[_]u8{50}, "lol"); // A collision with "hello"

    table.delete("hello");

    try expect(table.count == 2);
    try expect(table.size == 53);
    try expect(table.base_size == 50);

    try table.resize_up();

    try expectEqual(table.count, 2);
    try expect(table.size == 101);
    try expect(table.base_size == 100);
}

test "HashTable resize_down" {
    const allocator = testing.allocator;

    const table = try HashTable.new_sized(allocator, 100);
    defer table.deinit();

    table.insert("hello", "world");
    table.insert("foo", "bar");
    // TODO: Collisions can be computed by using get_hash instead. This will
    // make changing the hash algo less painful
    table.insert(&[_]u8{50}, "lol"); // A collision with "hello"

    table.delete("hello");

    try expect(table.count == 2);
    try expect(table.size == 101); // TODO: Replace with calls to prime fns probably
    try expect(table.base_size == 100);

    try table.resize_down();

    try expectEqual(table.count, 2);
    try expect(table.size == 53);
    try expect(table.base_size == 50);
}
