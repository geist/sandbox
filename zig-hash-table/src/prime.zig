const std = @import("std");
const math = std.math;
const testing = std.testing;
const expect = testing.expect;

// TODO: Make this generic
pub fn is_prime(number: u32) bool {
    if (number < 2) {
        return false;
    }
    if (number < 4) {
        return true;
    }
    if ((number % 2) == 0) {
        return false;
    }

    var i: u8 = 3;
    while (i <= math.sqrt(number)) : (i += 2) {
        if ((number % i) == 0) {
            return false;
        }
    }

    return true;
}

pub fn next_prime(number: u32) u32 {
    var new_number = number;
    while (!is_prime(new_number)) {
        new_number += 1;
    }

    return new_number;
}

test "is_prime" {
    try expect(is_prime(1) == false);
    try expect(is_prime(2) == true);
    try expect(is_prime(3) == true);
    try expect(is_prime(4) == false);
    try expect(is_prime(5) == true);
    try expect(is_prime(6) == false);
    try expect(is_prime(7) == true);
    try expect(is_prime(9) == false);
    try expect(is_prime(11) == true);
}

test "next_prime" {
    try expect(next_prime(1) == 2);
    try expect(next_prime(2) == 2);
    try expect(next_prime(3) == 3);
    try expect(next_prime(4) == 5);
    try expect(next_prime(6) == 7);
    try expect(next_prime(8) == 11);
    try expect(next_prime(9) == 11);
}
